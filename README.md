Priority message retriever
==========================

This is an implementation of the priority message retriever as described in
"Take home coding problem 1.pdf".

Interface
---------

### `Log`

The `Log` class holds on to the actual data for each log. Currently, this is
just the message and its priority, but it could be extended by adding fields and
getters for the date/time, thread and class name, etc. (4). The priority is
implemented as an enumeration (as `LOW`, `MEDIUM`, and `HIGH`) instead of as an
integer.

### `Logger`

The `Logger` class provides an interface for creating new log messages. Its
`log()` method immediately adds a new log with the given priority and message.
It also provides multiline/buffered messages via the `bufferedLog()` and
`bufferedLogWrite()` methods (2).

### `FixedPriorityLogger`

The `FixedPriorityLogger` class is a wrapper around `Logger` that allows you to
set a priority in its constructor. The priority can then be omitted in the calls
to the logging methods -- in this case, it will use the preset priority (1).

### `LogReader`

The `LogReader` class provides an interface to read logs via its `get()` method.
The ordering is the same as the one defined in the PDF (highest priority first,
then most recent first).

### `LogCollection`

The `LogCollection` class implements the actual storage of log entries using
three `ConcurrentLinkedDeque`s, one for each priority. This is an internal class
that is not intended for public consumption.

If any of the `ConcurrentLinkedDeque`s run out of memory while adding a `Log`,
the JVM will throw an `OutOfMemoryError` (3), which can be caught in client code
which can then retrieve some messages or free some memory in another way. If a
fixed capacity is desired instead, you could replace `ConcurrentLinkedDeque`
with `LinkedBlockingDeque`, which allows setting a fixed capacity and throws
`IllegalStateException` when trying to add one too many `Log`s.

Tests
-----

The tests are implemented using JUnit 4. You can build and run them with `gradle
test`.

* `BasicLogging`: Test basic logging
* `MultiLoggers`: Test multiple instances of `Logger`
* `FixedPriorityLoggers`: Test loggers that accept a preset priority in the
  constructor
* `BufferedLogging`: Test multiline logging
* `StressTest`: All of the above tests, 1 million times in a row. This helped me
  fix a bug in a previous iteration where logs that are too close in time could
  be sorted incorrectly.
* `MultiThreadedLogging`: Test logging from multiple threads. To get consistent
  results, each thread is responsible for logging one priority of message.

Building
--------

I wrote this using Gradle 4.4 and Java 8 via OpenJDK on Linux:

```
[aarmea@aarmea-ohm nautilus-test]$ javac -version
javac 1.8.0_144
[aarmea@aarmea-ohm nautilus-test]$ java -version
openjdk version "1.8.0_144"
OpenJDK Runtime Environment (build 1.8.0_144-b01)
OpenJDK 64-Bit Server VM (build 25.144-b01, mixed mode)
[aarmea@aarmea-ohm nautilus-test]$ javac -version
javac 1.8.0_144
[aarmea@aarmea-ohm nautilus-test]$ gradle -version

------------------------------------------------------------
Gradle 4.4
------------------------------------------------------------

Build time:   2017-12-06 09:05:06 UTC
Revision:     cf7821a6f79f8e2a598df21780e3ff7ce8db2b82

Groovy:       2.4.12
Ant:          Apache Ant(TM) version 1.9.9 compiled on February 2 2017
JVM:          1.8.0_144 (Oracle Corporation 25.144-b01)
OS:           Linux 4.14.3-1-ARCH amd64

```
