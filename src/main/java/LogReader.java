// Interface for retrieving logs.
public class LogReader {
  public LogReader() {}

  // Retrieves the message of the highest priority, most recent log and removes
  // it from the collection.
  public String get() {
    Log log = LogCollection.getInstance().getLog();
    if (log != null)
      return log.getMessage();
    else
      return null;
  }
}
