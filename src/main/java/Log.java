// An actual `Log` entry.
public class Log {
  public Log(Priority priority, String message) {
    this.priority = priority;
    this.message = message;
  }

  public Priority getPriority() {
    return priority;
  }

  public String getMessage() {
    return message;
  }

  // Specifies the order in which log entries are retrieved. `HIGH` priority
  // messages are always returned before `MEDIUM` priority messages, which are
  // always returned before `LOW` priority messages.
  public enum Priority {
    HIGH,
    MEDIUM,
    LOW
  }

  private Priority priority;
  private String message;
}
