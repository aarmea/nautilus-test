// Interface for creating logs without specifying the priority each time.
public class FixedPriorityLogger extends Logger {
  // Creates the logger. Its `priority` can be set at this time.
  public FixedPriorityLogger(Log.Priority priority) {
    this.priority = priority;
  }

  // Logs a message. Like Logger::log, but without the `priority`.
  public void log(String message) {
    log(priority, message);
  }

  // Writes a multiline message. Like Logger::bufferedLogWrite, but without the
  // `priority`. Because `FixedPriorityLogger` inherits `Logger`, `bufferedLog`
  // is still available and can be used to add to the buffer.
  public void bufferedLogWrite() {
    bufferedLogWrite(priority);
  }

  private Log.Priority priority;
}
