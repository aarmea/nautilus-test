import java.lang.System;

// Interface for creating new logs.
public class Logger {
  public Logger() {}

  // Logs a given `message` with the provided `priority`.
  public void log(Log.Priority priority, String message) {
    LogCollection.getInstance().addLog(new Log(priority, message));
  }

  // Appends the `part` to multiline message buffer.
  public void bufferedLog(String part) {
    messageBuffer.append(part);
  }

  // Writes the multiline message buffer to a new log with the `priority`.
  public void bufferedLogWrite(Log.Priority priority) {
    log(priority, messageBuffer.toString());
    messageBuffer = new StringBuilder();
  }

  private StringBuilder messageBuffer = new StringBuilder();
}
