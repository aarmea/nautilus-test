import java.util.concurrent.ConcurrentLinkedDeque;

// Thread-safe storage of `Log`s from multiple `Logger`s. Implemented as three
// `ConcurrentLinkedDeque` for thread-safety and better add/remove runtime vs.
// `PriorityBlockingQueue`.
class LogCollection {
  private LogCollection() {}

  // This is a singleton so that every `LogReader` can access `Log`s from every
  // `Logger`.
  public static LogCollection getInstance() {
    return instance;
  }

  // Saves a `Log` to the correct queue. This should run in amortized constant
  // time because adding to a queue is constant time.
  public void addLog(Log log) {
    switch(log.getPriority()) {
      case HIGH:
        hiPriLogs.addLast(log);
        break;
      case MEDIUM:
        medPriLogs.addLast(log);
        break;
      case LOW:
        lowPriLogs.addLast(log);
        break;
      default:
        break;
    }
  }

  // Retrieves a `Log` from the queue of the highest available priority. This
  // should run in constant time because removing from a queue is constant time.
  public Log getLog() {
    Log log = hiPriLogs.pollLast();
    if (log != null)
      return log;

    log = medPriLogs.pollLast();
    if (log != null)
      return log;

    log = lowPriLogs.pollLast();
    return log;
  }

  // Resets the collection. This is only intended for the unit tests, where this
  // is needed to prevent the results of one test from impacting another.
  static void reset() {
    instance = new LogCollection();
  }

  private static LogCollection instance = new LogCollection();
  private ConcurrentLinkedDeque<Log> hiPriLogs = new ConcurrentLinkedDeque<Log>();
  private ConcurrentLinkedDeque<Log> medPriLogs = new ConcurrentLinkedDeque<Log>();
  private ConcurrentLinkedDeque<Log> lowPriLogs = new ConcurrentLinkedDeque<Log>();
}
