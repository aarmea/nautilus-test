import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

public class LogTest {
  @After
  public void teardown() {
    // Prevent one failed test from impacting the others
    LogCollection.reset();
  }

  @Test
  public void BasicLogging() {
    Logger logger = new Logger();
    LogReader logReader = new LogReader();

    logger.log(Log.Priority.LOW, "abc");
    logger.log(Log.Priority.MEDIUM, "def");
    logger.log(Log.Priority.LOW, "ghi");
    logger.log(Log.Priority.HIGH, "jkl");
    logger.log(Log.Priority.HIGH, "mno");

    assertEquals("mno", logReader.get());
    assertEquals("jkl", logReader.get());
    assertEquals("def", logReader.get());
    assertEquals("ghi", logReader.get());
    assertEquals("abc", logReader.get());
    assertNull(logReader.get());
  }

  @Test
  public void MultiLoggers() {
    Logger logger1 = new Logger();
    Logger logger2 = new Logger();
    Logger logger3 = new Logger();
    LogReader logReader = new LogReader();

    logger1.log(Log.Priority.LOW, "abc");
    logger2.log(Log.Priority.MEDIUM, "def");
    logger3.log(Log.Priority.LOW, "ghi");
    logger1.log(Log.Priority.HIGH, "jkl");
    logger3.log(Log.Priority.HIGH, "mno");

    assertEquals("mno", logReader.get());
    assertEquals("jkl", logReader.get());
    assertEquals("def", logReader.get());
    assertEquals("ghi", logReader.get());
    assertEquals("abc", logReader.get());
    assertNull(logReader.get());
  }

  @Test
  public void FixedPriorityLoggers() {
    FixedPriorityLogger hiPriLogger = new FixedPriorityLogger(Log.Priority.HIGH);
    FixedPriorityLogger medPriLogger = new FixedPriorityLogger(Log.Priority.MEDIUM);
    FixedPriorityLogger lowPriLogger = new FixedPriorityLogger(Log.Priority.LOW);
    LogReader logReader = new LogReader();

    lowPriLogger.log("abc");
    medPriLogger.log("def");
    lowPriLogger.log("ghi");
    hiPriLogger.log("jkl");
    hiPriLogger.log("mno");

    assertEquals("mno", logReader.get());
    assertEquals("jkl", logReader.get());
    assertEquals("def", logReader.get());
    assertEquals("ghi", logReader.get());
    assertEquals("abc", logReader.get());
    assertNull(logReader.get());
  }

  @Test
  public void BufferedLogging() {
    Logger logger = new Logger();
    FixedPriorityLogger medPriLogger = new FixedPriorityLogger(Log.Priority.MEDIUM);
    LogReader logReader = new LogReader();

    logger.log(Log.Priority.LOW, "abc");
    logger.log(Log.Priority.MEDIUM, "def");
    logger.log(Log.Priority.LOW, "ghi");

    logger.bufferedLog("jkl");
    logger.bufferedLog("mno");
    logger.bufferedLogWrite(Log.Priority.HIGH);

    medPriLogger.bufferedLog("pqr");
    medPriLogger.bufferedLog("stu");
    medPriLogger.bufferedLogWrite();

    assertEquals("jklmno", logReader.get());
    assertEquals("pqrstu", logReader.get());
    assertEquals("def", logReader.get());
    assertEquals("ghi", logReader.get());
    assertEquals("abc", logReader.get());
    assertNull(logReader.get());
  }

  @Test
  public void StressTest() {
    final int STRESS_ITERATIONS = 1000000;

    for (int i = 0; i < STRESS_ITERATIONS; ++i) {
      BasicLogging();
    }

    for (int i = 0; i < STRESS_ITERATIONS; ++i) {
      MultiLoggers();
    }

    for (int i = 0; i < STRESS_ITERATIONS; ++i) {
      FixedPriorityLoggers();
    }

    for (int i = 0; i < STRESS_ITERATIONS; ++i) {
      BufferedLogging();
    }
  }

  @Test
  public void MultiThreadedLogging() throws Exception {
    final int LOG_REPEATS = 1000;
    final List<String> LOW_LOGS = Arrays.asList("abc", "def", "ghi", "jkl", "mno");
    final List<String> MED_LOGS = Arrays.asList("bcd", "efg", "hij", "klm", "nop", "qrs", "tuv");
    final List<String> HI_LOGS = Arrays.asList("cde", "fgh", "ijk");

    final Logger logger = new Logger();

    // One thread per priority
    ExecutorService executors = Executors.newFixedThreadPool(3);

    Future<Void> lowPriWriteThread = executors.submit(() -> {
      for (int i = 0; i < LOG_REPEATS; ++i)
        for (String log : LOW_LOGS)
          logger.log(Log.Priority.LOW, log);
      return null;
    });

    Future<Void> medPriWriteThread = executors.submit(() -> {
      for (int i = 0; i < LOG_REPEATS; ++i)
        for (String log : MED_LOGS)
          logger.log(Log.Priority.MEDIUM, log);
      return null;
    });

    Future<Void> hiPriWriteThread = executors.submit(() -> {
      for (int i = 0; i < LOG_REPEATS; ++i)
        for (String log : HI_LOGS)
          logger.log(Log.Priority.HIGH, log);
      return null;
    });

    // Synchronize so that the order becomes deterministic
    lowPriWriteThread.get();
    medPriWriteThread.get();
    hiPriWriteThread.get();

    // We're testing a LIFO queue, so we need to check these in reverse
    Collections.reverse(LOW_LOGS);
    Collections.reverse(MED_LOGS);
    Collections.reverse(HI_LOGS);

    LogReader logReader = new LogReader();

    // Finally, do the actual checking
    for (int i = 0; i < LOG_REPEATS; ++i)
      for (String log : HI_LOGS)
        assertEquals(log, logReader.get());

    for (int i = 0; i < LOG_REPEATS; ++i)
      for (String log : MED_LOGS)
        assertEquals(log, logReader.get());

    for (int i = 0; i < LOG_REPEATS; ++i)
      for (String log : LOW_LOGS)
        assertEquals(log, logReader.get());
  }
}
