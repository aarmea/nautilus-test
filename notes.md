Pre-coding notes
================

Broad strokes
* One `LogReader` object makes many `Logger` objects
* `LogReader` consumes `Log` objects, `Logger` produces `Log` objects
* `Log` objects sort first by their priority and second by their date
  * Implement this with a custom comparator on the class

Java notes
* `PriorityBlockingQueue` is a good building block
* JUnit for testing

Weird things to look out for
* Concurrency (how?)
* Wide Unicode characters
* Using priorities other than 1, 2, or 3 (throw an exception?)
* Runtime -- using any PriorityQueue variant is O(log(n)) for insert and
  removal, so consider using separate queues for each priority

Project is a basic command-line Gradle application
* Generated with `gradle init --type java-application`
* Run with `gradle run` (this won't do anything too useful)
* Test with `gradle test`

Classes
* `Log` -- the actual log itself, holds a priority, message, and date
* `Logger` -- produces `Log`s
* `LogReader` -- consumes `Log`s

Singleton
---------

It needs to be a singleton to satisfy the requirement that the LogReader reads
the messages from all Logger instances

Non-blocking implementation
---------------------------

### Old
While the project relies on the `PriorityBlockingQueue` class, my implementation
does not use any of the blocking retrieval operators, and as such, does not
block on read. Because it uses System.nanoTime instead of a counter to sort
entries and the `PriorityBlockingQueue` class is logically unbounded, my
implementation does not block on write either.

### Current
The project relies on the `ConcurrentLinkedDeque` class, which does not block on
read or write, to store actual log entries. In comparison to the implementation
using `PriorityBlockingQueue`, this implementation has the benefit of
constant-time insertion and deletion, but it loses the ability to log priorities
other than the three specified.
